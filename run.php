<?php

$config = include __DIR__ . '/config.php';

$Resource = pg_connect( $config['connection'] );


echo "\n --- Update DB action --- \n\n";


$Result = pg_query( $Resource , "SELECT description FROM pg_description WHERE  objoid = 'system.db_update_history'::regclass;");
 
if( $Result != false && count($Result) == 1 ){
	 
	
	$Row = pg_fetch_assoc( $Result );
	$description = $Row['description'];
	$queriesDir = __DIR__ . '/Executables/';
	 
	$listQueries = scandir( $queriesDir , SCANDIR_SORT_ASCENDING );
	
	$ok = true;
	
	echo 'STARTING TRANSACTION'.PHP_EOL;
	
	pg_query("BEGIN") or die("Could not start transaction\n");
	
	foreach ( $listQueries as $inputFile ){

		if( ($inputFile != '.harmless_tracker' ) && ($inputFile > "$description.php") ){

			echo "\n --- Update -->> #". pathinfo($inputFile,PATHINFO_FILENAME ) ."# --- \n\n";
			 
			include $queriesDir.'/'.$inputFile;
			
			$ClassName = 'Executables\\'.pathinfo($inputFile,PATHINFO_FILENAME );
			$Class = new $ClassName( $Resource );
			
			if( ! $Class->run() ){
				
				$ok = false;
				break;
			}
			
			echo "\nUpdate flag -> ";
				
			$Result = pg_query( $Resource , "COMMENT ON TABLE system.db_update_history IS '".pathinfo($inputFile,PATHINFO_FILENAME ) ."'" );
				
			if( ! $Result ){
				
				$ok = false;
				break;
			}
			 
			echo "\n\n --- End -->> #". pathinfo($inputFile,PATHINFO_FILENAME ) ."# --- \n\n";
			
			unset($Class);
		}
	}
	
	if( $ok ) {
		
		echo "COMMITING TRANSACTION".PHP_EOL;
		pg_query("COMMIT") or die("Transaction commit failed\n");
	} else {
		echo "ROLLING BACK TRANSACTION".PHP_EOL;
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	

	
} else {
	 
	echo 'Something not work here...';
}



echo "\n\n --- ----------------- --- \n";